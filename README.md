A (currently: *mock*) pipeline to run amplicon stage 1 and 2 pipelines.

# Input
BCL files on lustre

# Output
various files on S3

# Parameters
- `--bcl_dir` Path to BCL files (in `Data/Intensities/BaseCalls/...` subdirectories). Required but not actually used in mock-up.
- `--manifest` Path to the manifeast file. Required but not actually used in mock-up.
- `--results_dir` Path of the `output` subdirectory of the stage2 amplicon pipeline.
- `--bucket` The S3 bucket to write to (eg `amplicon_pipeline`), followed by `/`, and a unique ID, possibly the UUID from the API, followed by `/`. The '/' at the end is important, otherwise all files will just be copied to that "file", overwriting each other.

# Example
```
nextflow run main.nf \
--bcl_dir '/lustre/scratch118/malaria/team112/personal/mm6/aspis-demo-data/29770/Data/Intensities/BaseCalls/L001/C263.1' \
--manifest '/lustre/scratch118/malaria/team112/personal/mm6/aspis-demo-data/29770/dummy_manifest.file' \
--results_dir '/lustre/scratch118/malaria/team112/personal/mm6/amplicon/32069/output' \
--bucket 'amplicon_pipeline/32069/'
```
Files will be written to that "directory" (S3 doesn't really have that concept, but close enough). You can check the output with
```
s3cmd ls s3://amplicon_pipeline/32069/
```
To clear the output:
```
s3cmd rm s3://amplicon_pipeline/32069/*
```

# copy_s3_local_validate.py
Helper script.
Copies a "directory" from S3 to local storage (not recursive at the moment), using `s3cmd sync`.
Will check the file size and MD5 sum of each file to ensure the copy is correct.
Will print "OK" to STDOUT if successful (returns 0), or an error to STDERR (returns 1).

## Setup
```
virtualenv -p python3 myenv
source myenv/bin/activate
pip3 install -r requirements.txt
```

## Example
```
./copy_s3_local_validate.py s3://amplicon_pipeline/mock_input/ /lustre/scratch118/malaria/team112/personal/mm6/aspis-demo-data/mock_tmp
```

## Test
```
source myenv/bin/activate
nosetests test_copy_s3_local_validate.py
```