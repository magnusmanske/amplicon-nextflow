process upload_to_s3 {
    /**
    Puts the given file(s) into the specific S3 bucket
    */
    input:
        path(target_files)

    script:
        bucket = params.bucket
        """
        echo ${target_files}
        s3cmd put ${target_files} s3://${bucket} -F
        """
}
