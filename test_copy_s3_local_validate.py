import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '.'))
import copy_s3_local_validate as c
from pathlib import Path
import shutil
from nose.tools import assert_raises

def test_calculate_md5_for_local_file():
    # check proper function
    md5 = c.calculate_md5_for_local_file('test_data/manifext.csv')
    assert md5 == '378176763b94c6d6b043097c28acf18f'

    # check non-existing file
    assert_raises(Exception,c.calculate_md5_for_local_file,'THIS_FILE_DOES_NOT_EXIST')

def test_ensure_directory_exists():
    directory_name = 'test_data/tmp123'
    if os.path.isdir(directory_name):
        shutil.rmtree(directory_name)
    assert not os.path.isdir(directory_name)
    c.ensure_directory_exists(directory_name)
    assert os.path.isdir(directory_name)
    shutil.rmtree(directory_name)
    assert not os.path.isdir(directory_name)

def test_test_archive():
    c.test_archive('test_data/test_data.tar','tar')
    c.test_archive('test_data/test_data.tar.gz','tar.gz')
    c.test_archive('test_data/test_data.zip','zip')

    # Unknown archive type
    assert_raises(Exception,c.test_archive,'test_data/test_data.tar','something else entirely')

    # Wrong archive type
    assert_raises(Exception,c.test_archive,'test_data/test_data.tar','zip')

    # Non-existing file
    assert_raises(Exception,c.test_archive,'THIS_FILE_DOES_NOT_EXIST','tar')

def test_fix_extraction_directory():
    base_dir = "test_data/rec1"
    if os.path.isdir(base_dir):
        shutil.rmtree(base_dir)
    os.makedirs(f"{base_dir}/rec2/rec3/rec4")
    Path(f"{base_dir}/rec2/rec3/rec4/dummy.txt").touch()
    c.fix_extraction_directory(base_dir)
    assert os.path.isfile(f"{base_dir}/dummy.txt")
    shutil.rmtree(base_dir)

def test_extract_archive():
    base_dir = "test_data/extract"
    for (archive,type) in [
        ("test_data/test_data.tar","tar"),
        ("test_data/test_data.tar.gz","tar.gz"),
        ("test_data/test_data.zip","zip")]:
        if os.path.isdir(base_dir):
            shutil.rmtree(base_dir)
        os.makedirs(base_dir)
        c.extract_archive(archive,type,base_dir)
        assert os.path.isfile(f"{base_dir}/test_data/manifext.csv")
    shutil.rmtree(base_dir)
