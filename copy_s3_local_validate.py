#!/usr/bin/env python3

import os
import sys
import subprocess
from subprocess import Popen, PIPE
import re
import hashlib
import uuid
import shutil

def calculate_md5_for_local_file(file_name) -> str:
    """Calculates the MD5 hashsum for a file, and returns the hex value
    """
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def ensure_directory_exists(directory_name):
    """Creates a new directory and all required parent directories, if necessary.
    """
    os.makedirs(directory_name, exist_ok=True)

# Not tested as relying on s3cmd and connection to Sanger S3.
def ensure_s3cmd_is_available():
    """Tests is `s3cmd` is available. Raises exception if not.
    """
    # For python > 3.6, one could use
    # result = subprocess.run(['s3cmd'], check=True, capture_output=True).decode('utf-8')
    # Until then...
    result = subprocess.run(['s3cmd'], stdout=subprocess.PIPE)
    output = result.stdout.decode('utf-8')
    if not output.startswith('Usage: s3cmd [options] COMMAND [parameters]'):
        raise Exception("s3cmd is not available")

# Not tested as relying on s3cmd and connection to Sanger S3.
def sync_from_s3(source_path,target_path):
    """Runs the `s3cmd sync` sub-command.
    Assumes that s3cmd exists; see ensure_s3cmd_is_available().
    """
    # For python > 3.6 use:
    # subprocess.run(['s3cmd','sync',source_path,target_path], check=True, capture_output=True)
    subprocess.run(['s3cmd','sync',source_path,target_path], stdout=subprocess.PIPE)

# Not tested as relying on s3cmd and connection to Sanger S3.
def s3_file_size_md5_generator(s3_path) -> list:
    """Generates a list of files in an S3 "directory".
    Yields [filesize,md5sum,filename] lists.
    Filenames are not prefixed with the path.
    """
    # For python > 3.6 use:
    # result = subprocess.run(['s3cmd','ls','--list-md5',s3_path], check=True, capture_output=True)
    result = subprocess.run(['s3cmd','ls','--list-md5',s3_path], stdout=subprocess.PIPE)
    output = result.stdout.decode('utf-8')
    for row in output.split('\n'):
        # Example:
        # 2021-06-09 15:02     92160   f773de33701b38184a2891f0e05184e5  s3://amplicon_pipeline/test_data.tar
        m = re.match(r"^\S+\s+\S+\s+(\d+)\s+([0-9a-f]+)\s.+/(.+)$",row)
        if not m:
            continue
        file_size,md5_sum,file_name = m.groups()
        yield (int(file_size),md5_sum,file_name)

# Not tested as relying on s3cmd and connection to Sanger S3.
def validate_s3_against_local(source_path,target_path):
    """Goes through the files in an S3 "directory", and compares them to a local copy.
    Checks file existence, file size, and md5sum.
    """
    for (file_size_s3,md5_sum_s3,file_name_s3) in s3_file_size_md5_generator(source_path):
        # Check if local file exists
        file_name_remote = os.path.join(source_path,file_name_s3)
        file_name_local = os.path.join(target_path, file_name_s3)
        if not os.path.isfile(file_name_local):
            raise Exception(f"{file_name_remote} was not copied to local")
        # Check if local file has the same size
        file_size_local = os.path.getsize(file_name_local)
        if int(file_size_local) != int(file_size_s3):
            raise Exception(f"{file_name_remote} is {file_size_s3} bytes, but {file_name_local} is {file_size_local} bytes")
        # Check if local file has the same MD5
        md5_sum_local = calculate_md5_for_local_file(file_name_local)
        if md5_sum_local != md5_sum_s3:
            raise Exception(f"{file_name_remote} has MD5 {md5_sum_s3}, but {file_name_local} has {md5_sum_local}")
        # Files are identical, AFAICT

def test_archive(filename,archive_type):
    """Tests validity of a tar (plain or gzipped) or zip archive.
    Raises an exception if the archive is not valid.
    """
    if archive_type=='tar':
        cmd = ["tar","-tf",filename]
    elif archive_type=='tar.gz':
        cmd = ["tar","-ztf",filename]
    elif archive_type=='tgz':
        cmd = ["tar","-ztf",filename]
    elif archive_type=='zip':
        cmd = ["unzip","-t",filename]
    else:
        raise Exception(f"Unknown archive type {archive_type}")

    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    stdout = stdout.decode('utf-8')
    stderr = stderr.decode('utf-8')
    if archive_type=='zip' and "Either this file is not" in stdout:
        stderr = stdout
    if stderr:
        raise Exception(stderr)

def extract_archive(filename,archive_type,target_path):
    """Extracts a tar (plain or gzipped) or zip archive.
    """
    if archive_type=='tar':
        cmd = ["tar","-xf",filename,"--directory",target_path]
    elif archive_type=='tar.gz':
        cmd = ["tar","-zxf",filename,"--directory",target_path]
    elif archive_type=='tgz':
        cmd = ["tar","-zxf",filename,"--directory",target_path]
    elif archive_type=='zip':
        cmd = ["unzip",filename,"-d",target_path] # TODO test
    else:
        raise Exception(f"Can not extract from file {source_path}, archive type {archive_type}")
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    stderr = stderr.decode('utf-8')
    if stderr:
        raise Exception(stderr)

def fix_extraction_directory(path):
    """Checks is the given directory contains no files, and only one subdirectory.
    If so, it moves all files in the subdirectory one level up,
    and deletes the (now empty) directory.
    This is done recursively, until there is a file, more than one subdirectory, or nothing
    in the "root" directory.
    """
    files = os.listdir(path)
    if len(files)!=1:
        return
    full_path = os.path.join(path,files[0])
    if not os.path.isdir(full_path):
        return
    
    # Move single directory to new UUID name
    uuid_path = os.path.join(path,str(uuid.uuid4()))
    os.rename(full_path,uuid_path)

    # Move all files in that directory one level up; collision unlikely because UUID
    file_names = os.listdir(uuid_path)
    for file_name in file_names:
        shutil.move(os.path.join(uuid_path, file_name), path)

    # Clean up empty UUID directory
    os.rmdir(uuid_path)

    # Do this again (might be more single directory levels)
    fix_extraction_directory(path)
    
    
# Not tested as relying on s3cmd and connection to Sanger S3.
def download_and_extract_from_s3(source_path,target_path,archive_type):
    """Downloads an archive file from S3, checks its integrity,
    unpacks it, and removes the archive file.
    """
    local_archive = os.path.join(target_path,str(uuid.uuid4())+"."+archive_type)
    sync_from_s3(source_path,local_archive)
    test_archive(local_archive,archive_type)
    extract_archive(local_archive,archive_type,target_path)
    os.remove(local_archive)
    fix_extraction_directory(target_path)

# Not tested as relying on s3cmd and connection to Sanger S3.
def download_directory_from_s3(source_path,target_path):
    """Downloads all files in an S3 "directory", and validates the downloaded files
    via file size and MD5 sum.
    Does not process subdirectories.
    """
    source_path = source_path.rstrip('/')+'/'
    sync_from_s3(source_path,target_path)
    validate_s3_against_local(source_path,target_path)

if __name__ == "__main__":
    # Check command line parameters
    if len(sys.argv) != 3:
        print(f"USAGE: {sys.argv[0]} S3_PATH LOCALPATH\nCopies from S3 to local and verifies (MD5 and size)", file = sys.stderr )
        exit(1)
    source_path = sys.argv[1]
    target_path = sys.argv[2]
    target_path = target_path.rstrip('/')+'/'

    # Prepare
    ensure_s3cmd_is_available()
    ensure_directory_exists(target_path)

    # Download and extraxt single archive file, or download/verify entire directory
    result = re.match(r"^.*\.(zip|tar|tgz|tar\.gz)$",source_path)
    if result:
        download_and_extract_from_s3(source_path,target_path,str(result.groups(0)[0]).lower())
    else:
        download_directory_from_s3(source_path,target_path)
    print ("OK")
