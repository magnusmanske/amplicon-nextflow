#!/usr/bin/env bash

# Get run ID
if [ -z ${1+x} ]; then
echo "The first variable needs to be the run identifier (eg a UUID)"
exit
fi
run_id=$1

# Check that $TOWER_ACCESS_TOKEN is set
if [ -z ${TOWER_ACCESS_TOKEN+x} ]; then
echo "TOWER_ACCESS_TOKEN needs to be set in environment"
exit
fi

tower_api=${TOWER_API:-"http://nf-tower.cellgeni.sanger.ac.uk/api"}
COMPUTE_ENVIRONMENT_ID=${TOWER_COMPUTE_ENVIRONMENT_ID:-"5eEKu5XE5oPweEOUkPZcAi"}
s3_bucket=${AMPLICON_PIPELINE_S3_BUCKET:-"amplicon_pipeline"}
input_root_path="/lustre/scratch118/malaria/team112/personal/mm6/aspis-demo-data/29770" # TODO construct from $run_id or something
timestamp=$(date "+%Y-%m-%dT%H:%M:%SZ")

# results_dir is just important for the mock pipeline, won't be needed for the real one
read -r -d '' JSON_DATA << EOM
{"launch":{
"id": "$run_id",
"computeEnvId": "$COMPUTE_ENVIRONMENT_ID",
"pipeline": "https://gitlab.com/magnusmanske/amplicon-nextflow",
"workDir": "/lustre/scratch118/malaria/team112/personal/mm6/nextflow_tower",
"configProfiles": ["sanger_lsf"],
"configText": "",
"paramsText": "{\"input_dir\":\"$input_root_path\",\"results_dir\": \"/lustre/scratch118/malaria/team112/personal/mm6/amplicon/32069/output\",\"bucket\": \"$s3_bucket/$run_id/\"}",
"preRunScript": "newgrp malariagendev\nmodule load nextflow/20.10.0-5430\nexport HTTP_PROXY=\"http://wwwcache.sanger.ac.uk:3128\"\nexport HTTPS_PROXY=$HTTP_PROXY",
"postRunScript": "",
"mainScript": "main.nf",
"entryName": "",
"schemaName": "",
"resume": false,
"pullLatest": true,
"stubRun": false,
"dateCreated": "$timestamp"
}}
EOM

# Write the JSON to a temporary file and invoke the tower API to create a workflow; workflow ID will be printed to STDOUT as JSON
json_file="/tmp/$run_id.json" # Construct temporary JSON file name
echo "$JSON_DATA" > $json_file # Write JSON data to file
curl -H "Authorization: Bearer $TOWER_ACCESS_TOKEN" -X POST "$tower_api/workflow/launch" --header "Content-Type: application/json" -d @"${json_file}"
rm -f $json_file # Clean up temporary JSON file
