#!/usr/bin/env nextflow

// enable dsl2
nextflow.preview.dsl = 2

// import modules
include { upload_to_s3 as upload_grc_to_s3 } from './modules/upload_to_s3.nf'
include { upload_to_s3 as upload_grc2_to_s3 } from './modules/upload_to_s3.nf'
include { upload_to_s3 as upload_plate_qc_plot_to_s3 } from './modules/upload_to_s3.nf'
include { upload_to_s3 as upload_species_plot_to_s3 } from './modules/upload_to_s3.nf'

// helper functions
def printHelp() {
    log.info """
    Usage: TODO
        nextflow run main.nf --manifest [manifest path] --bcl_dir [BCL directory] --reference [ref genome fasta path] --results_dir [output dir]
    """.stripIndent()
}

workflow {
    // Show help message
    if (params.help) {
        printHelp()
        exit 0
    }

    // Check input directory
    if (params.bcl_dir) {
    	bcl_path=file(params.bcl_dir)
        if (bcl_path.exists()) {
            println("Using existing bcl directory: $bcl_path")
        } else {
            exit 1, "You must supply the path to AN EXISTING directory with BCL files using the --bcl_dir option"
        }
    } else {
        exit 1, "You must supply the path to a directory with BCL files using the --bcl_dir option"
    }

    // Check manifest file
    if (params.manifest) {
    	manifest_file=file(params.manifest)
        if (manifest_file.exists()) {
            println("Using existing manifest file: $manifest_file")
        } else {
            exit 1, "You must supply the path to AN EXISTING manifest file using the --manifest option"
        }
    } else {
        exit 1, "You must supply the path to a manifest file using the --manifest option"
    }

    // Create results directory
    if (params.results_dir) {
	results_path=file(params.results_dir)
        if (results_path.exists()) {
            println("Using existing results dir: $results_path")
        } else {
	    made_dir = results_path.mkdir()
	    if (made_dir) {
                println("Successfully made results dir: $results_path")
            } else {
                exit 1, "Failed to make results dir: $results_path. Check you have permissions to create the dir."
            }
        }
    } else {
        exit 1, "You must supply the path to a directory for storing pipeline output (using the --results_dir option)"
    }

    // Upload outputs to S3
    // There are more files in the output directory, not sure what else in required as output
    // This could probably just be one upload_to_s3 command, as it should take multiple parameters?
    upload_grc_to_s3("${results_path}/GRC/_GRC.txt")
    upload_grc2_to_s3("${results_path}/GRC/_GRC2.txt")
    upload_plate_qc_plot_to_s3("${results_path}/Plots/_plateQC.pdf")
    upload_species_plot_to_s3("${results_path}/Plots/_species.pdf")
}
